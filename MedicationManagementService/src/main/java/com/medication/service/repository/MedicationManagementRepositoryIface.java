package com.medication.service.repository;


import com.medication.service.model.PatientDetails;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MedicationManagementRepositoryIface extends MongoRepository<PatientDetails, Integer> {

}

