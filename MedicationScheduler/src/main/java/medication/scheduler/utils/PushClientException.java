package medication.scheduler.utils;

public class PushClientException extends Exception {
    public PushClientException(Exception e) {
        super(e);
    }
}
