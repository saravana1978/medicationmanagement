package medication.scheduler.schedulers;


import lombok.AllArgsConstructor;
import static medication.scheduler.constants.Constants.TIME_ZONE;


import lombok.extern.slf4j.Slf4j;
import medication.scheduler.services.NotificationService;

import medication.scheduler.utils.PushClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@AllArgsConstructor

public class SchedulerService {

    @Autowired
    private NotificationService notificationService;


    @Scheduled(cron = "${cron.notification.expression}", zone = TIME_ZONE)
    public void sendEmailNotification() throws PushClientException, InterruptedException {
        notificationService.sendNotification();

    }
}
