package medication.scheduler.constants;

public class Constants {

    public static final String TIME_ZONE = "US/Central";
    public static final String PROCESS_NAME_NOTIFICATION = "SEND_NOTIFICATION";
    public static final String DATE_PATTERN = "'date:'yyyy-MM-dd 'time:'HH:mm 'month:'MMM 'day-of-week:'EEE 'week-of-month:'W";

}
