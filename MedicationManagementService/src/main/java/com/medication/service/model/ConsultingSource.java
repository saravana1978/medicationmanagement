package com.medication.service.model;

public class ConsultingSource {
    private String consultingHospitalName;
    private String consultingDoctorName;

    public ConsultingSource(String consultingHospitalName, String consultingDoctorName) {
        this.consultingHospitalName = consultingHospitalName;
        this.consultingDoctorName = consultingDoctorName;
    }

    public String getConsultingHospitalName() {
        return consultingHospitalName;
    }

    public void setConsultingHospitalName(String consultingHospitalName) {
        this.consultingHospitalName = consultingHospitalName;
    }

    public String getConsultingDoctorName() {
        return consultingDoctorName;
    }

    public void setConsultingDoctorName(String consultingDoctorName) {
        this.consultingDoctorName = consultingDoctorName;
    }
}
