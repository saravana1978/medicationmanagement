package com.medication.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicationManagementService {

	public static void main(String[] args) {
		SpringApplication.run(MedicationManagementService.class, args);
	}

}
