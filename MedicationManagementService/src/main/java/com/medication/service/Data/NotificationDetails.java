package com.medication.service.Data;


public class NotificationDetails {
      private String medicineName ;
      private String scheduleStartDate;

    public NotificationDetails(String medicineName, String scheduleStartDate) {
        this.medicineName = medicineName;
        this.scheduleStartDate = scheduleStartDate;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public String getScheduleStartDate() {
        return scheduleStartDate;
    }

    public void setScheduleStartDate(String scheduleStartDate) {
        this.scheduleStartDate = scheduleStartDate;
    }
}
