package medication.scheduler.entities;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "BACKLOG_STATUS")
public class BacklogStatus {
    private String status;
    private String id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BacklogStatus(String status, String id) {
        this.status = status;
        this.id = id;
    }
}
