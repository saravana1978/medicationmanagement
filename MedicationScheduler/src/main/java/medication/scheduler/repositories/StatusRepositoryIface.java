package medication.scheduler.repositories;


import medication.scheduler.entities.BacklogStatus;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;


import java.util.List;

public interface StatusRepositoryIface extends MongoRepository<BacklogStatus, Integer> {



    List<BacklogStatus> findByStatus(String status);
}
