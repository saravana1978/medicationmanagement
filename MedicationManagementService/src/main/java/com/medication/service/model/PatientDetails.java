package com.medication.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Document(collection = "PATIENT_DETAILS")
public class PatientDetails {

    @Id
    private String id;
    private String patientName;
    private Long mobileNumber;
    private String medicineName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy'T'HH:mm:ssXXX")
    private Calendar scheduleStartDate;

    public PatientDetails(String id, String patientName, Long mobileNumber, String medicineName, Calendar scheduleStartDate) {
        this.id = id;
        this.patientName = patientName;
        this.mobileNumber = mobileNumber;
        this.medicineName = medicineName;
        this.scheduleStartDate = scheduleStartDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public Long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(Long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public Calendar getScheduleStartDate() {
        return scheduleStartDate;
    }

    public void setScheduleStartDate(Calendar scheduleStartDate) {
        this.scheduleStartDate = scheduleStartDate;
    }


}
