package com.medication.service.resource;

import java.util.List;


import com.medication.service.Data.NotificationDetails;
import com.medication.service.model.PatientDetails;
import com.medication.service.services.PatientDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MedicationManagementController {

	@Autowired
	PatientDetailsService patientDetailsService;



	@GetMapping("/findAllPatientDetails")
	public List<PatientDetails> getPatientDetails() {
		return patientDetailsService.getPatientDetails();
	}

	@PostMapping("/addPatientDetailsHM")
	public String savePatientFromHM(@RequestBody NotificationDetails notificationDetails) {
		return patientDetailsService.savePatientFromHM(notificationDetails);
	}

	@PostMapping("/addPatientDetailsApps")
	public String savePatientFromApps(@RequestBody NotificationDetails notificationDetails) {
		return patientDetailsService.savePatientFromApps(notificationDetails);
	}


	@GetMapping("/deletePatientDetails/{id}")
	public String deletePatient(@PathVariable int id) {
		 return patientDetailsService.deletePatient(id);
	}
}
