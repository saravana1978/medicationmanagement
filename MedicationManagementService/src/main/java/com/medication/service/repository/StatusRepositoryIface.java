package com.medication.service.repository;

import com.medication.service.model.BacklogStatus;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StatusRepositoryIface extends MongoRepository<BacklogStatus, Integer> {
}
