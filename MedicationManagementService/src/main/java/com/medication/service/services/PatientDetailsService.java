package com.medication.service.services;

import com.medication.service.Data.NotificationDetails;
import com.medication.service.model.BacklogStatus;
import com.medication.service.model.PatientDetails;
import com.medication.service.repository.MedicationManagementRepositoryIface;
import com.medication.service.repository.StatusRepositoryIface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Service
public class PatientDetailsService {
    @Autowired
    MedicationManagementRepositoryIface medicationManagementRepositoryIface;
    @Autowired
    StatusRepositoryIface statusRepositoryIface;

    public List<PatientDetails> getPatientDetails() {
        return medicationManagementRepositoryIface.findAll();
    }


    public String savePatientFromHM(NotificationDetails notificationDetails) {
        PatientDetails patientDetails=new PatientDetails(null, null, null, notificationDetails.getMedicineName(), dateConverter(notificationDetails.getScheduleStartDate()));

        medicationManagementRepositoryIface.save(patientDetails);
        if(patientDetails.getId()!=null){
        BacklogStatus backlogStatus=new BacklogStatus("NEW",patientDetails.getId());
            statusRepositoryIface.save(backlogStatus);
        }
        return "Added Patient from HM with Id--" + patientDetails.getId();
    }


    public String savePatientFromApps( NotificationDetails notificationDetails) {

        PatientDetails patientDetails=new PatientDetails(null, null, null, notificationDetails.getMedicineName(), dateConverter(notificationDetails.getScheduleStartDate()));

        medicationManagementRepositoryIface.save(patientDetails);
        if(patientDetails.getId()!=null){
            BacklogStatus backlogStatus=new BacklogStatus("NEW",patientDetails.getId());
            statusRepositoryIface.save(backlogStatus);
        }
        return "Added Patient from App with Id--" + patientDetails.getId();
    }


    public String deletePatient(int id) {
        medicationManagementRepositoryIface.deleteById(id);
        return "Patient deleted with id--- "+id;
    }

    private Calendar dateConverter(String str){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
       try{ Date date = sdf.parse(str);
           Calendar cal = Calendar.getInstance();
           cal.setTime(date);
           return cal;
       }catch (Exception e){
           Calendar cal = Calendar.getInstance();
           cal.setTime(null);
           return cal;
        }
    }
}
