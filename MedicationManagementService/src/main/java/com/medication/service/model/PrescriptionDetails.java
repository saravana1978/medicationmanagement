package com.medication.service.model;

import java.time.LocalTime;
import java.util.Calendar;

public class PrescriptionDetails {

    private String medicineName;
    private String medicineType;
    private int dose;
    private Calendar scheduleStartDate;
    private LocalTime timeToTake;
     private float duration;
    private String frequency;

    public PrescriptionDetails(String medicineName, String medicineType, int dose, Calendar scheduleStartDate, LocalTime timeToTake, float duration, String frequency) {
        this.medicineName = medicineName;
        this.medicineType = medicineType;
        this.dose = dose;
        this.scheduleStartDate = scheduleStartDate;
        this.timeToTake = timeToTake;
        this.duration = duration;
        this.frequency = frequency;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public String getMedicineType() {
        return medicineType;
    }

    public void setMedicineType(String medicineType) {
        this.medicineType = medicineType;
    }

    public int getDose() {
        return dose;
    }

    public void setDose(int dose) {
        this.dose = dose;
    }

    public Calendar getScheduleStartDate() {
        return scheduleStartDate;
    }

    public void setScheduleStartDate(Calendar scheduleStartDate) {
        this.scheduleStartDate = scheduleStartDate;
    }

    public LocalTime getTimeToTake() {
        return timeToTake;
    }

    public void setTimeToTake(LocalTime timeToTake) {
        this.timeToTake = timeToTake;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }
}
